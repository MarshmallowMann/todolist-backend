const express = require("express");
const postController = require("../controllers/postControllers");
const router = express.Router();

router.route("/").get(postController.getAllTodos).post(postController.newTodo);

router
  .route("/:id")
  .get(postController.getTodo)
  .put(postController.updateTodo)
  .delete(postController.deleteTodo);

module.exports = router;
