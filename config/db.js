require("dotenv").config();
const mysql = require("mysql2");

// Create a connection to the database
const connection = mysql.createConnection({
  host: process.env.DB_HOST || "localhost",
  user: process.env.DB_USER || "root",
  database: process.env.DB_NAME || "simple-todo",
  password: process.env.DB_PASSWORD || "",
});

// let sql = `SELECT * FROM todos`;
// connection.execute(sql, function (err, results) {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(results);
//   }
// });

module.exports = connection.promise();
