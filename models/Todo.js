const db = require("../config/db");

class Todo {
  constructor(title, status) {
    this.title = title;
    this.status = status;
  }

  async save() {
    let date = new Date();

    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    let created_at = `${year}-${month}-${day}`;

    let sql = `INSERT INTO todos (title, status, created_at) VALUES (?, ?, ?);`;
    let [newTodo, _] = await db.execute(sql, [
      this.title,
      this.status,
      created_at,
    ]);
    return newTodo;
  }

  static findAll() {
    let sql = `SELECT * FROM todos;`;
    return db.execute(sql);
  }

  static findById(id) {
    let sql = `SELECT * FROM todos WHERE id = ?;`;
    return db.execute(sql, [id]);
  }

  static updateById(id, status) {
    let sql = `UPDATE todos SET status = ? WHERE id = ?;`;
    return db.execute(sql, [status, id]);
  }

  static deleteById(id) {
    let sql = `DELETE FROM todos WHERE id = ?;`;
    return db.execute(sql, [id]);
  }
}

module.exports = Todo;
