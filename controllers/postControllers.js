const Todo = require("../models/Todo");

exports.getAllTodos = (req, res) => {
  try {
    Todo.findAll()
      .then(([todos, _]) => {
        console.log("Success in getting all todos");
        res.status(200).json(todos);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({ message: "Something went wrong" });
      });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

exports.getTodo = (req, res) => {
  let id = req.params.id;

  try {
    Todo.findById(id)
      .then(([todo, _]) => {
        console.log(todo);
        res.status(200).json(todo);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({ message: "Something went wrong" });
      });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

exports.newTodo = (req, res) => {
  let title = req.body.title;
  let status = parseInt(req.body.status);

  let todo = new Todo(title, status);

  if (!title) {
    console.log("Title is required");
    return res.status(400).json({ message: "Title is required" });
  }

  if (![0, 1].includes(status)) {
    console.log("Status must be 0 or 1");
    return res.status(400).json({ message: "Status must be 0 or 1" });
  }

  console.log(todo);

  todo
    .save()
    .then((savedTodo) => {
      console.log(savedTodo);
      res.status(201).json(savedTodo);
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ message: "Something went wrong" });
    });
};

exports.updateTodo = (req, res) => {
  // update status of a todo to true or false
  let id = req.params.id;

  // get the todo from the database
  Todo.findById(id).then(([todo, _]) => {
    let status = todo[0].status === 0 ? 1 : 0;

    // update the todo
    Todo.updateById(id, status)
      .then(([updatedTodo, _]) => {
        console.log(updatedTodo);
        res.status(200).json(updatedTodo);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({ message: "Something went wrong" });
      });
  });
};

exports.deleteTodo = (req, res) => {
  let id = req.params.id;

  Todo.deleteById(id)
    .then(([deletedTodo, _]) => {
      console.log(deletedTodo);
      res.status(200).json(deletedTodo);
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ message: "Something went wrong" });
    });
};

// Path: routes/postRoutes.js
